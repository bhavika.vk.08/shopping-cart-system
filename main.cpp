#include <iostream>

using namespace std;

class product
{
private:
    char name[10];
    int cost;
    int no;
    static int icount;
public:
    static int getcount()
    {
        return icount;
    }
    void adddetails()
    {
        cout<< "\nEnter product name\n";
        cin>>name;
        cout<< "\nEnter price of the product\n";
        cin>>cost;
        cout<< "Enter the number of pieces\n";
        cin>>no;
        icount++;
    }
    void display(int n)
    {
        cout<<n<< "\t"<<name<< "\t"<<cost<< "\t"<<no<<"\n";
    }
    void getname()
    {
        cout<<name;
    }
    void buyproduct(int n)
    {
        no=no-n;
    }
    void removefromcart(int n)
    {
        no=no+n;
    }
    int getprice()
    {
        return cost;
    }
};

int product:: icount;
void manageproducts(product S[]);
void addproduct(product S[]);
void displaystore(product S[]);
void shopping(product S[]);
void displaycart(product S[],int cart[]);
void bill(product S[],int cart[]);

int main()
{
    product store[100];
    int choice;
    while (1)
    {
        //system("clear");
        cout<<"Welcome to shopping\n\n ";
        cout<<"1.Seller\n\n2. Customer\n\n3.Exit";
        cout<<"\nEnter your choice:\n";
        cin>>choice;
        switch(choice)
        {
            case 1: cout<<"\nYou can update your products";
                    manageproducts(store);
                    break;
            case 2: cout<<"\n\nWelcome to the store\n\n";
                    shopping(store);
                    break;

            case 3: return 0;
        }



    }

}

void manageproducts(product S[])
{
    int choice;
    while(1)
    {
        cout<<"\n1.Add product\n2.Check products in store\n3.Exit\n";
        cout<<"Enter choice\n";
        cin>>choice;
        switch(choice)
        {
            case 1 :S[product::getcount()].adddetails();
                    break;
            case 2 :displaystore(S);
                    break;
            case 3 :return;
        }
    }

}

void displaystore(product S[])
{
    int i,n=product::getcount();
    cout<<"\n\nCODE\tPRODUCT NAME\tPRICE\tNO. OF ITEMS\t\n";
    for (i=0;i<n;i++)
    {
        S[i].display(i);
    }
}

void shopping(product S[])
{
    int cart[product::getcount()];
    int choice,code,i;
    for (i=0;i<product::getcount();i++)
        cart[i]=0;
    while (1)
    {
        cout<< "\n1.Add product to cart\n2.Remove product from cart\n3.View cart\n4.Bill\n5.Exit";
        cout<<"\nEnter choice\n";
        cin>>choice;
        switch(choice)
        {
            case 1 :displaystore(S);
                    cout<< "Enter code of product\n";
                    cin>>code;
                    cout<< "Enter number of items\n";
                    cin>>cart[code];
                    S[code].buyproduct(cart[code]);
                    break;
            case 2 :displaycart(S,cart);
                    cout<< "Enter code of object to be removed";
                    cin>>code;
                    cout<< "Enter number of objects to be removed";
                    cin>>i;
                    cart[code]=cart[code]-i;
                    S[code].removefromcart(i);
                    break;
            case 3 :displaycart(S,cart);
                    break;
            case 4 :bill(S,cart);
                    break;
            case 5 :return;
        }
    }
}

void displaycart(product S[],int cart[])
{
    int i,n;
    n=product::getcount();
    cout<< "Code\tName of product\tPrice\tNumber of items\n";
    for (i=0;i<n;i++)
    {
        if(cart[i]!=0)
        {
            cout<<i<<"\t";
            S[i].getname();
            cout<< "\t"<<S[i].getprice()<< "\t"<<cart[i];
        }
    }
}

void bill(product S[],int cart[])
{
    int total=0,i,n;
    n=product::getcount();
    for (i=0;i<n;i++)
    {
        total=total+cart[i]*S[i].getprice();
    }
    cout<< "\ntotal amount to be paid is "<<total;
}
